# FROM rockylinux:8
FROM rockylinux:9 as build
ARG version
ENV version=$version
ARG jemalloc
ENV jemalloc=$jemalloc
ARG slash_version
ENV slash_version=$slash_version
ARG liburing
ENV liburing=$liburing

ENV ACLOCAL_PATH="/usr/local/share/aclocal"
ENV VARNISHSRC="/varnish-cache/"

WORKDIR /
RUN sed 's/https/http/g' -i /etc/yum.repos.d/*repo
#RUN dnf install -y 'dnf-command(config-manager)' 
RUN dnf install http://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm -y \
    && dnf upgrade -y \
    && sed 's/https/http/g' -i /etc/yum.repos.d/*repo \
    && dnf install -y 'dnf-command(config-manager)' \
    && dnf config-manager --set-enabled crb  \
    && yum install -y diffutils \
        make \
        autoconf \
        autoconf-archive \
        automake \
        libedit-devel \
        libtool \
        ncurses-devel \
        pcre2-devel \
        python3-docutils \
	python3-pip \
        cpio \
        git \
        cmake \
        m4 \
	    mhash-devel \
        xxhash-devel \
    && pip3 install sphinx \
    && yum update -y 

RUN git clone https://github.com/jemalloc/jemalloc.git \
    && git clone https://github.com/maxmind/libmaxminddb.git \
    && git clone https://github.com/varnishcache/varnish-cache 

WORKDIR /libmaxminddb
RUN ./bootstrap\
    && ./configure --disable-tests \
    && make -j$(nproc) install
WORKDIR /jemalloc
RUN  git checkout $jemalloc \
    && ./autogen.sh ;./configure ;  make -j$(nproc) ; make install_bin install_include install_lib \
    && echo /usr/local/lib >> /etc/ld.so.conf; ldconfig
WORKDIR /varnish-cache
RUN git checkout varnish-$version \
    &&	./autogen.sh \
    && ./configure \
    && make -j$(nproc)  install
WORKDIR /
RUN git clone --recursive https://github.com/varnishcache-friends/libvmod-geoip2 \
    && git clone https://gitlab.com/uplex/varnish/slash.git 
WORKDIR /libvmod-geoip2
ENV PKG_CONFIG_PATH="/usr/local/lib/pkgconfig"
RUN ./autogen.sh \
    && ./configure \
    &&    make -j$(nproc) \
    && make install
ENV ACLOCAL_PATH="/usr/local/share/aclocal"
ENV VARNISHSRC="/varnish-cache/"
WORKDIR /slash
RUN  dnf install liburing-devel -y
#RUN  ./bootstrap --disable-tests \
RUN  ./bootstrap \
    && ldconfig \
    && git checkout $slash_version \
    && make -j$(nproc) install
WORKDIR /
COPY . /

WORKDIR /usr/local/bin
RUN ls -l / \
&& cp /varnish_reload_vcl /usr/local/bin/ -f \
&& cp /varnishReloader.sh /usr/local/bin/ -f \
&& cp /docker-entrypoint.sh /usr/local/bin/ -f \
&& chmod +x /usr/local/bin/*

FROM rockylinux:9-minimal
COPY --from=build /usr/local /usr/local
RUN  echo /usr/local/lib >> /etc/ld.so.conf \
   && ldconfig \
   && rpm -i http://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm  \
   && sed 's/https/http/g' -i /etc/yum.repos.d/*repo \
   && microdnf update -y \
   && microdnf install geolite2-country libedit inotify-tools jemalloc gcc findutils  libedit xxhash-libs liburing libaio  -y
ENV TERM=linux
ARG force
ENV build_date=$force
RUN echo $force
COPY varnish_run /usr/local/bin
WORKDIR /etc/varnish
ENTRYPOINT [ "/usr/local/bin/docker-entrypoint.sh" ]

