#!/bin/bash
###########

while true
do
  inotifywait  --excludei '(.*.swp$|.*.swx$|.*.temporary$|.*.download$|.*.md5$|.*.tmp.*|.*.txt)' -e create -e modify -e delete -e move -r /etc/varnish 

  # ansible will create a file at the begining of its vcl updates, this will wait for it to be removed
  while [ -f /etc/varnish/vcl_wait ];do sleep 10;done

  # to stop possible race on files managed by the cdn-agent process where they are created but ar not finished being written to disk we will sleep for 10 seconds
  sleep 10

  until /usr/local/bin/varnish_reload_vcl; do
    # loop on reload until it works
    sleep 10
  done  
 
done
