BUILD_VERSION := 1
version := 7.6.1

JEMALLOC := 5.3.0
SLASH_VER := b7cc99fcbadd7b912204f7e11405f0e09d56b8b0
BUILD_TAG := $(version)_$(JEMALLOC)_$(BUILD_VERSION)
liburing := true
force := $(shell date --iso=seconds)

# Automatically determine whether to use podman or docker
CONTAINER_RUNTIME := $(shell command -v podman || command -v docker)

build:
	$(CONTAINER_RUNTIME) build -t docker.io/jlcox1970/varnish:$(BUILD_TAG) \
		--build-arg liburing=$(liburing) \
		--build-arg version=$(version) \
		--build-arg force=$(force) \
		--build-arg jemalloc=$(JEMALLOC) \
		--build-arg slash_version=$(SLASH_VER) \
		--security-opt=label=disable .

buildx:
	$(CONTAINER_RUNTIME) manifest create docker.io/jlcox1970/varnish:$(BUILD_TAG)
	$(CONTAINER_RUNTIME) manifest create docker.io/jlcox1970/varnish:$(version)
	$(CONTAINER_RUNTIME) buildx build --platform linux/amd64,linux/arm64 --manifest docker.io/jlcox1970/varnish:$(BUILD_TAG) \
		--build-arg liburing=$(liburing) \
		--build-arg version=$(version) \
		--build-arg force=$(force) \
		--build-arg jemalloc=$(JEMALLOC) \
		--build-arg slash_version=$(SLASH_VER) \
		--security-opt=label=disable .
	$(CONTAINER_RUNTIME) buildx build --platform linux/amd64,linux/arm64 --manifest docker.io/jlcox1970/varnish:$(version) \
		--build-arg liburing=$(liburing) \
		--build-arg version=$(version) \
		--build-arg force=$(force) \
		--build-arg jemalloc=$(JEMALLOC) \
		--build-arg slash_version=$(SLASH_VER) \
		--security-opt=label=disable .

export: build
	$(CONTAINER_RUNTIME) image save docker.io/jlcox1970/varnish:$(BUILD_TAG) -o varnish-latest.tgz

push: build
	$(CONTAINER_RUNTIME) push docker.io/jlcox1970/varnish:$(BUILD_TAG)

pushx: buildx
	$(CONTAINER_RUNTIME) manifest push docker.io/jlcox1970/varnish:$(BUILD_TAG)
	$(CONTAINER_RUNTIME) manifest push docker.io/jlcox1970/varnish:$(version)

